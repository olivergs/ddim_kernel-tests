---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml

.rules:
  no-merge-train:
    if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
    when: never
  mr-parent:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID == $CI_MERGE_REQUEST_PROJECT_ID
  mr-fork:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID
  default-branch:
    if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH =~ /^redhat/

.rules-codeowners:
  extends: .rules
  mr-parent:
    changes: [CODEOWNERS]
  default-branch:
    changes: [CODEOWNERS]

# override the default CKI workflow rules to only run in very specific
# circumstances as forks don't have the tagged runners available
workflow:
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, mr-fork]
    - !reference [.rules, default-branch]

default:
  tags:
    - kernel-qe-internal-runner

# explicitly fail MR pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/insufficient-permissions
    strategy: depend
  rules:
    - !reference [.rules, mr-fork]

shell check cki:
  extends: .cki_tools
  before_script:
    - "# Mock beakerlib.sh to avoid source import errors:"
    - mkdir /usr/share/beakerlib
    - touch /usr/share/beakerlib/beakerlib.sh
  script:
    - shellcheck --external-sources --format=gcc cki_*/*.sh
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]

shell check runtests:
  extends: .cki_tools
  script:
    - find -name '*.sh' -exec shellcheck --severity=error -f gcc {} +
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]

codeowners check:
  extends: .cki_tools
  script:
    - |
      # compare CODEOWNERS
      export GITLAB_TOKENS='{"gitlab.com":"GITLAB_JOB_CODEOWNERS_KERNEL_TESTS_MR_ACCESS_TOKEN"}'
      if ! python3 -m cki.deployment_tools.gitlab_codeowners_config \
          ${CI_MERGE_REQUEST_IID:+--comment-mr-iid "${CI_MERGE_REQUEST_IID}"} \
          needs-update
      then
          python3 -m pip install git+https://gitlab.com/cki-project/kpet.git
          git clone https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git --depth 1
          python3 -m kpet --db kpet-db test list -o json > kpet-db-tests.json
          python3 -m cki.deployment_tools.gitlab_codeowners_config \
              --test-path kpet-db-tests.json \
              ${CI_MERGE_REQUEST_IID:+--comment-mr-iid "${CI_MERGE_REQUEST_IID}"} \
              diff
      fi
  rules:
    - !reference [.rules-codeowners, mr-parent]
    - !reference [.rules-codeowners, default-branch]

check bash with mixed tabs and spaces:
  extends: .cki_tools
  script:
    - |
      mixed=$(comm -12 \
          <(find . -type f -iname '*.sh' -exec grep -lPe '^\t' {} + | sort) \
          <(find . -type f -iname '*.sh' -exec grep -lPe '^ ' {} + | sort))
      if [[ -n "${mixed}" ]]; then
          echo "Files with mixed tabs/spaces:"
          echo "${mixed}" | sed 's/^/  /'
          exit 1
      fi
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]

check bash lines ending with white spaces:
  extends: .cki_tools
  script:
    - |
      whitespaces=$((find . -type f -iname '*.sh' -exec grep -lPe '\s$' {} + || true ) | sort)
      if [[ -n "${whitespaces}" ]]; then
          echo "Files that have lines ending with whitespaces:"
          echo "${whitespaces}" | sed 's/^/  /'
          echo "for more details run: grep -ne '\s$' <filename>"
          exit 1
      fi
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]

check if LTP version on DCI matches the default on distribution/ltp/include/ltp-make.sh:
  extends: .cki_tools
  script:
    - |
      # Check the DCI test versions
      for rhel_version in 7 8; do
          function rlIsRHEL() { [[ "$1" = "${rhel_version}" ]]; }; source distribution/ltp/include/ltp-make.sh
          dci_xml="dci/rhel${rhel_version}.xml"
          if ! grep -q ltp-full-${TESTVERSION}.tar.bz2 "${dci_xml}"; then
              echo "FAIL: needs to update LTP version on ${dci_xml} to ${TESTVERSION}"
              exit 1
          fi
      done
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, default-branch]

.git-sync:
  extends: .cki_tools
  stage: 🚀
  variables:
    GIT_DEPTH: 100  # works for MRs <= 100 commits on top of the target branch
    INTERNAL_GITLAB_HOST: gitlab.cee.redhat.com
    INTERNAL_GITLAB_PROJECT: kernel-qe/kernel-tests-public
    COMMENT_TEMPLATE_J2: |
      {% set suite_url = 'https://' +
        env['INTERNAL_GITLAB_HOST'] +
        '/api/v4/projects/' +
        (env['INTERNAL_GITLAB_PROJECT'] | replace('/', '%2f')) +
        '/repository/archive.zip?sha=refs/heads/' +
        env['TARGET_BRANCH']
      -%}
      {% set mr_url = env['CI_MERGE_REQUEST_PROJECT_URL'] +
        '/-/merge_requests/' +
        env['CI_MERGE_REQUEST_IID']
      -%}
      The updated code is synced to the [internal mirror]({{ suite_url }}).

      <details>
      <summary>Click here for an example on how to trigger a Beaker job with it.</summary>

      ```xml
      <job>
        <whiteboard>Beaker job for {{ mr_url }}</whiteboard>
        <recipeSet>
          <recipe ks_meta="redhat_ca_cert">
            <distroRequires>
              <distro_arch op="=" value="x86_64"/>
              <variant op="=" value="BaseOS"/>
              <distro_family op="=" value="CentOSStream9"/>
            </distroRequires>
            <hostRequires/>
            <task name="/test/misc/machineinfo">
              <fetch url="{{ suite_url }}#test/misc/machineinfo"/>
              <params/>
            </task>
          </recipe>
        </recipeSet>
      </job>
      ```
      </details>
  script:
    - git push --force
      "https://git:${GITLAB_JOB_MIRROR_KERNEL_TESTS_MIRROR_ACCESS_TOKEN}@${INTERNAL_GITLAB_HOST}/${INTERNAL_GITLAB_PROJECT}.git"
      "HEAD:refs/heads/$TARGET_BRANCH"
    - |
      if [[ -n "${CI_MERGE_REQUEST_PROJECT_ID:-}" ]]; then
        # delete old bot notes
        bot_user_id=$(
            gitlab \
                --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
                --output json \
                --fields id \
                current-user get \
            | jq \
                --raw-output \
                '.id'
        )
        gitlab \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            --output json \
            --fields id,author \
            project-merge-request-note list \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --all \
        | jq \
            --raw-output \
            --arg user_id "${bot_user_id}" \
            '.[] | select(.author.id == ($user_id | tonumber)) | .id' \
        | xargs \
            --max-args 1 \
            --no-run-if-empty \
            gitlab \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            project-merge-request-note delete \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --id
        # post a new note with the URL
        body=$(python3 -m cki.deployment_tools.render <<< "${COMMENT_TEMPLATE_J2}")
        gitlab \
            --private-token "${GITLAB_JOB_MIRROR_KERNEL_TESTS_COMMENT_ACCESS_TOKEN}" \
            project-merge-request-note create \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --body "${body}"
      fi

git-sync-mr:
  extends: .git-sync
  variables:
    TARGET_BRANCH: merge-requests/$CI_MERGE_REQUEST_IID
  rules:
    - !reference [.rules, no-merge-train]
    - !reference [.rules, mr-parent]

git-sync-default-branch:
  extends: .git-sync
  variables:
    TARGET_BRANCH: $CI_COMMIT_BRANCH
  rules:
    - !reference [.rules, default-branch]
